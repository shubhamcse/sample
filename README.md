Design Consideration:

1) Used MMVM pattern with Viewmodels to solve the problem statement  
2) Error screnario design considered but not handled to show in the app right now  
3) Models are only createde for gameName and image fetching, rest model classes are empty  
4) Custom Deserializer used to parse response (GSON)  
5) Retrofit is used for networking  
6) Glide for downoading the images  
7) Android lifecycle components LiveData used  
8) Usit test is not written for every class right now, mainly for repository only(time limitation)  
9) Mockk is used for testing    
10) Multiple branches not used as this was just a sample project  
   
   
If Time Permits:    

1) More edge cases could be handled, dependency injection framework can be used if we think project is going to be big    
2) More Unit tests & UI tests    
3) Error caes can be gracefully handled with retry option     