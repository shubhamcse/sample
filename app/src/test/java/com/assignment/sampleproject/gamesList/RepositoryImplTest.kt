package com.assignment.sampleproject.gamesList

import com.assignment.sampleproject.utils.APIService
import com.assignment.sampleproject.utils.ErrorResponse
import com.assignment.sampleproject.utils.RepositoryImpl
import com.assignment.sampleproject.utils.ResultWrapper
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class RepositoryImplTest {

    private val dispatcher = TestCoroutineDispatcher()
    private lateinit var repositoryImpl: RepositoryImpl

    @RelaxedMockK
    private lateinit var apiService: APIService

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        repositoryImpl = RepositoryImpl(apiService)
    }

    @Test
    fun `when lambda returns successfully then it should emit the result as success`() {
        runBlockingTest {
            val lambdaResult = true
            val result = repositoryImpl.createAPICall(dispatcher) { lambdaResult }
            assertEquals(ResultWrapper.Success(lambdaResult), result)
        }
    }

    @Test
    fun `when lambda throws IOException then it should emit the result as NetworkError`() {
        runBlockingTest {
            val result = repositoryImpl.createAPICall(dispatcher) { throw IOException() }
            assertEquals(ResultWrapper.NetworkError, result)
        }
    }

    @Test
    fun `when lambda throws HttpException then it should emit the result as GenericError`() {

        val errorBody = ResponseBody.create(MediaType.get("application/json"), "{\"message\": \"Unexpected parameter\"}")

        runBlockingTest {
            val result = repositoryImpl.createAPICall(dispatcher) {
                throw HttpException(Response.error<Any>(422, errorBody))
            }
            assertEquals(ResultWrapper.GenericError(422, ErrorResponse("Unexpected parameter")), result)
        }
    }

    @Test
    fun `when lambda throws unknown exception then it should emit GenericError`() {
        runBlockingTest {
            val result = repositoryImpl.createAPICall(dispatcher) {
                throw IllegalStateException()
            }
            assertEquals(ResultWrapper.GenericError(), result)
        }
    }
}
