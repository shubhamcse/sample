package com.assignment.sampleproject.gamesList

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.utils.Repository
import com.assignment.sampleproject.utils.ResultWrapper
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule


@ExperimentalCoroutinesApi
class GamesViewModelTest {

    @get:Rule var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    lateinit var repository: Repository

    lateinit var gamesViewModel: GamesViewModel

    @Before
    fun setUp(){
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun `when fetchGames called then it should call getGames on repository`() {
        val game:ResultWrapper<Games> = mockk()
        coEvery { repository.getGames() }  returns game
        gamesViewModel = GamesViewModel(repository)
        runBlocking {
            gamesViewModel.fetchGamesList()
        }
        coVerify { repository.getGames() }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}
