package com.assignment.sampleproject.utils

import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.assignment.sampleproject.utils.ResultWrapper

interface Repository {
   suspend fun getGames() : ResultWrapper<Games>
}
