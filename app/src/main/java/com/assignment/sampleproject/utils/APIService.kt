package com.assignment.sampleproject.utils

import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.gamesList.models.GamesItem
import retrofit2.http.GET

interface APIService {
    @GET("https://api.unibet.com/game-library-rest-api/getGamesByMarketAndDevice.json?jurisdiction=UK&brand=unibet&deviceGroup=mobilephone&locale=en_GB&currency=GBP&categories=casino,softgames&clientId=casinoapp")
    suspend fun getGames(): Games
}
