package com.assignment.sampleproject.utils

import com.assignment.sampleproject.BuildConfig
import com.assignment.sampleproject.gamesList.CustomGamesDesrializer
import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.assignment.sampleproject.gamesList.models.GamesMap
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object APIClient {
    private const val BASE_URL = BuildConfig.SERVER_URL

    private fun getRetrofit(): Retrofit {
        val builder = GsonBuilder()
        builder.registerTypeAdapter(GamesMap::class.java, CustomGamesDesrializer())
        val gson = builder.create()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    val apiService: APIService = getRetrofit().create(APIService::class.java)
}
