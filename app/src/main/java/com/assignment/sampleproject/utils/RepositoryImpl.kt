package com.assignment.sampleproject.utils

import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException


class RepositoryImpl(
    private val service: APIService,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : Repository {

    override suspend fun getGames(): ResultWrapper<Games> {
        return createAPICall(dispatcher) { service.getGames() }
    }

     suspend fun <T> createAPICall(dispatcher: CoroutineDispatcher, apiCall: suspend () -> T): ResultWrapper<T> {
        return withContext(dispatcher) {
            try {
                ResultWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> ResultWrapper.NetworkError
                    is HttpException -> {
                        val code = throwable.code()
                        val errorResponse = convertErrorBody(throwable)
                        ResultWrapper.GenericError(code, errorResponse)
                    }
                    else -> {
                        ResultWrapper.GenericError(null, null)
                    }
                }
            }
        }
    }

    private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
        return try {
           throwable.response()?.errorBody()?.source()?.let {
               val gsonBuilder = GsonBuilder()
               val gson = gsonBuilder.create()
               gson.fromJson(it.readUtf8(), ErrorResponse::class.java)
            }
        } catch (exception: Exception) {
            null
        }
    }

}
