package com.assignment.sampleproject.gamesList

import android.os.Bundle
import android.view.View
import android.view.View.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.assignment.sampleproject.databinding.ActivityMainBinding
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.assignment.sampleproject.utils.APIClient
import com.assignment.sampleproject.utils.RepositoryImpl
import com.assignment.sampleproject.utils.Status

class MainActivity : AppCompatActivity() {

    private val gamesViewModel : GamesViewModel by lazy {
        ViewModelProviders.of(
            this,
            GamesViewModelFactory(RepositoryImpl(APIClient.apiService))
        )
            .get(GamesViewModel::class.java)
    }

    private val binding:ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    private val gamesAdapter by lazy { GamesAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        getGamesList()
        setRecyclerView()
    }

    private fun getGamesList(){
        gamesViewModel.getGamesListState().observe(this, { gamesListState ->
            gamesListState.let {
                when (it.status) {
                    Status.LOADING -> {
                        showLoadingView()
                    }
                    Status.SUCCESS -> {
                        setData(it.data?.games?.gamesMap)
                    }
                    Status.ERROR -> {
                        //show error message, and can show retry option
                    }
                }

            }
        })
    }

    private fun setRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2)
        with(binding) {
           recyclerView.layoutManager = layoutManager
           recyclerView.adapter = gamesAdapter
        }
    }

    private fun showLoadingView() {
        with(binding) {
            indeterminateBar.visibility = VISIBLE
            recyclerView.visibility = GONE
        }
    }

    private fun setData(gamesMap: HashMap<String, GamesItem>?) {
        with(binding) {
            indeterminateBar.visibility = GONE
            recyclerView.visibility = VISIBLE
        }
        gamesAdapter.setData(gamesMap)
    }
}
