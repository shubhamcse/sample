package com.assignment.sampleproject.gamesList

import android.util.Log
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.assignment.sampleproject.utils.Repository
import com.assignment.sampleproject.utils.Resource
import com.assignment.sampleproject.utils.ResultWrapper
import kotlinx.coroutines.launch

class GamesViewModel(private val repository: Repository) : ViewModel() {

    val gamesListState = MutableLiveData<Resource<Games>>()

    init {
        fetchGamesList()
    }

    @VisibleForTesting
    fun fetchGamesList() {
        viewModelScope.launch {
            gamesListState.postValue(Resource.loading(null))
            when(val result = repository.getGames()){
                is ResultWrapper.Success -> {
                    gamesListState.postValue(Resource.success(result.value))
                }
                is ResultWrapper.GenericError -> {
                    gamesListState.postValue(Resource.error(null, result.error?.message))
                }
                is ResultWrapper.NetworkError -> {
                    gamesListState.postValue(Resource.error(null, null))
                }
            }
        }
    }

    fun getGamesListState(): LiveData<Resource<Games>> {
        return gamesListState
    }
}
