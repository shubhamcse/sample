package com.assignment.sampleproject.gamesList.models

import com.google.gson.annotations.SerializedName

data class GamesItem (
    val gameName: String,
    val imageUrl: String,
    val backgroundImageUrl: String
)

