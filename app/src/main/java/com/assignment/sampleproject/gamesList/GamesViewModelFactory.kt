package com.assignment.sampleproject.gamesList

import androidx.lifecycle.*
import com.assignment.sampleproject.utils.Repository

class GamesViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GamesViewModel::class.java)) {
            return GamesViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown VM name")
    }

}
