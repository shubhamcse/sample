package com.assignment.sampleproject.gamesList

import com.assignment.sampleproject.gamesList.models.Games
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.assignment.sampleproject.gamesList.models.GamesMap
import com.google.gson.*
import java.lang.reflect.Type


class CustomGamesDesrializer :
        JsonDeserializer<GamesMap> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type?,
                             context: JsonDeserializationContext?): GamesMap {
        val result: HashMap<String, GamesItem> = HashMap()
        val games = json.asJsonObject
        val gsonBuilder = GsonBuilder()
        val gson = gsonBuilder.create()
        /* val gamesObject = games.getAsJsonObject("games")*/
            for ((key, value1) in games.entrySet()) {
                val value = gson.fromJson(value1, GamesItem::class.java)
                result[key] = value
            }

        return GamesMap(result)
    }
}
