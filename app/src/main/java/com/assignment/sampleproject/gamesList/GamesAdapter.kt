package com.assignment.sampleproject.gamesList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.assignment.sampleproject.R
import com.assignment.sampleproject.gamesList.models.GamesItem
import com.bumptech.glide.Glide

class GamesAdapter() :
        RecyclerView.Adapter<GamesAdapter.GamesViewHolder>() {

    private var mData = HashMap<String, GamesItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesViewHolder {
        return GamesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.game_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: GamesViewHolder, position: Int) {
        val gameMap = mData.entries.elementAt(position)
        val gameItem = gameMap.value
        holder.bind(gameItem)

    }

    fun setData(data: HashMap<String, GamesItem>?) {
        if (data != null) {
            mData = data
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mData.keys.size
    }

    inner class GamesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var gameName: TextView = itemView.findViewById(R.id.text_game_name)
        var image: ImageView = itemView.findViewById(R.id.image_game)

        fun bind(gamesItem: GamesItem) {
            gameName.text = gamesItem.gameName

            Glide.with(itemView.context)
                    .load(gamesItem.imageUrl)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(image)
        }
    }
}
